USE cine;

drop table peliculas;
drop table generos;

create table generos(
GEN_ID integer NOT NULL PRIMARY KEY(GEN_ID) IDENTITY,
GEN_DESCRIPTION VARCHAR(50)
);

create table peliculas(
PEL_ID integer NOT NULL PRIMARY KEY(PEL_ID) IDENTITY,
GEN_ID integer FOREIGN KEY REFERENCES GENEROS(GEN_ID),
GEN_DESCRIPTION VARCHAR(50)
);

